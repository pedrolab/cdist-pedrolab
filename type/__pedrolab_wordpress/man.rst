work in progress

parameter is domain to deploy

after running this cdist type you will need to configure the form

vars are hardcoded: wordpress as user password and database in localhost host

after that, you put user, site name, etc.

it would be interesting to use wp-cli to finish the automation
==============================================================

https://www.nginx.com/blog/automating-installation-wordpress-with-nginx-unit-on-ubuntu/
https://gist.github.com/nginx-gists/bdc7da70b124c4f3e472970c7826cccc

if [ ! -f /usr/local/bin/wp ]; then
  # Install the WordPress CLI
  echo "▶ Installing the WordPress CLI tool"
  curl --retry 6 -Ls "https://github.com/wp-cli/wp-cli/releases/download/v${WORDPRESS_CLI_VERSION}/wp-cli-${WORDPRESS_CLI_VERSION}.phar" > /usr/local/bin/wp
  echo "$WORDPRESS_CLI_MD5 /usr/local/bin/wp" | md5sum -c -
  chmod +x /usr/local/bin/wp
fi


https://gist.github.com/nginx-gists/bdc7da70b124c4f3e472970c7826cccc

another interesting settings
============================


# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN set -eux; \
	docker-php-ext-enable opcache; \
	{ \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=2'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

https://github.com/docker-library/wordpress/blob/cdb836237e3af7bfd011957316f159c1e81bf29c/latest/php7.4/fpm/Dockerfile#L62-L72
