#!/bin/sh -e

NGINX_CONFIG="$(cat <<EOF
server {
  listen 80;
  listen [::]:80;
  server_name ${DOMAIN};

  location /.well-known {
    default_type "text/plain";
    allow all;
    root /var/www/html;
  }

  location / {
    return 301 https://${DOMAIN}\$request_uri;
  }
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name ${DOMAIN};

  #ssl_certificate /etc/letsencrypt/live/${DOMAIN}/fullchain.pem;
  #ssl_certificate_key /etc/letsencrypt/live/${DOMAIN}/privkey.pem;
  ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
  ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

  client_max_body_size 200M;

  # Your only path reference. -> src https://www.nginx.com/resources/wiki/start/topics/recipes/wordpress/
  root /var/www/html/wordpress;
  ## This should be in your http block and if it is, it's not needed here.
  index index.php;

  # https://www.wordfence.com/help/firewall/optimizing-the-firewall#hide-userini-nginx
  location ~ ^/\.user\.ini {
    deny all;
  }

  location = /favicon.ico {
    log_not_found off;
    #access_log off;
  }

  location = /robots.txt {
    allow all;
    log_not_found off;
    #access_log off;
  }

  # Remove X-Powered-By, which is an information leak
  fastcgi_hide_header X-Powered-By;

  gzip on;
  gzip_vary on;
  gzip_comp_level 4;
  gzip_min_length 256;
  gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
  #gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;
  gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

  location / {
    # This is cool because no php is touched for static content.
    # include the "?\$args" part so non-default permalinks doesn't break when using query string
    try_files \$uri \$uri/ /index.php?\$args;
  }

  location ~ \.php$ {

    # # alternate method from https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/
    #     include snippets/fastcgi-php.conf;
    #     fastcgi_pass unix:/run/php/php7.0-fpm.sock;

    # # 2021-2-23 temp disable cache
    # fastcgi_cache phpcache; # The name of the cache key-zone to use
    # fastcgi_cache_valid 200 30m; # What to cache: 'Code 200' responses, for half an hour
    # fastcgi_cache_methods GET HEAD; # What to cache: only GET and HEAD requests (not POST)
    # add_header X-Fastcgi-Cache \$upstream_cache_status; # Add header so we can see if the cache hits or misses

    #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
    include fastcgi_params;
    fastcgi_intercept_errors on;
    # src https://www.nginx.com/resources/wiki/start/topics/examples/phpfcgi/
    fastcgi_pass unix:/run/php/php7.3-fpm.sock;
    #The following parameter can be also included in fastcgi_params file
    fastcgi_param  SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
  }

  location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
    # inspiration in lang thing -> https://stackoverflow.com/questions/46307578/how-can-i-make-multi-language-config-in-nginx
    # semicolon problem -> https://www.petefreitag.com/item/831.cfm -> https://stackoverflow.com/questions/14684463/curly-braces-and-from-apache-to-nginx-rewrite-rules
    rewrite "^/([a-z]{2})(/.*)?$" /\$2;
    expires max;
  }

}
EOF
)"
