#!/bin/sh -e

NGINX_CONFIG="$(cat <<EOF
server {
  listen 80;
  listen [::]:80;
  server_name ${DOMAIN};

  location /.well-known {
    default_type "text/plain";
    allow all;
    root /var/www/html;
  }

  location / {
    return 301 https://${DOMAIN}\$request_uri;
  }
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name ${DOMAIN};

  #ssl_certificate /etc/letsencrypt/live/${DOMAIN}/fullchain.pem;
  #ssl_certificate_key /etc/letsencrypt/live/${DOMAIN}/privkey.pem;
  ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
  ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;

  client_max_body_size 1G;

$( [ -n ${CUSTOM_LOGO} ] && cat <<EOF2
  #location = /logo_title_white.fc0639fd838e4d93ca09.svg {
  location = /assets/logo_title_white.svg {
    alias /var/www/html/thingsboard-custom-logo.svg;
  }
EOF2
)

  location / {
    proxy_pass http://localhost:8080;
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;

    # critical parameters for websockets
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";

    # extras, supposedly "good UX for websockets"
    # this service uses websockets and is a good inspiration -> extra https://pve.proxmox.com/wiki/Web_Interface_Via_Nginx_Proxy
    # here you see websockets for 1 hour -> https://thingsboard.io/docs/user-guide/install/pe/add-haproxy-ubuntu/
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_buffering off;
    client_max_body_size 0;
    proxy_connect_timeout  3600s;
    proxy_read_timeout  3600s;
    proxy_send_timeout  3600s;
    send_timeout  3600s;
  }
}
EOF
)"
