#!/bin/sh -e

# based on __pedrolab_prometheus type

os="$(cat "${__global}/explorer/os")"
case "${os}" in
  debian)
  ;;
  *)
    echo "Your OS '${os}' is currently not supported." > /dev/stderr
    exit 1
  ;;
esac

# if version is not defined by user, go to default latest stable version according to this manifest
VERSION_PARAM="${__object}/parameter/version"
if [ -f "${VERSION_PARAM}" ]; then
  GITEA_SHOULD_VERSION="$(cat "$VERSION_PARAM")"
  GITEA_CHECKSUM_URL="https://github.com/go-gitea/gitea/releases/download/v${GITEA_SHOULD_VERSION}/gitea-${GITEA_SHOULD_VERSION}-linux-amd64.sha256"
  GITEA_CHECKSUM="sha256:$(curl -sS -L "${GITEA_CHECKSUM_URL}" | awk '{print $1}')"
else
  GITEA_SHOULD_VERSION=1.14.2
  GITEA_CHECKSUM='sha256:0d11d87ce60d5d98e22fc52f2c8c6ba2b54b14f9c26c767a46bf102c381ad128'
fi

USER="gitea"
GROUP="gitea"
GITEA_DIR="/var/lib/gitea"
GITEA_CFG_DIR="/etc/gitea"

# TODO switch between sqlite and postgresql
# __package postgresql

__user gitea --create-home --home /home/gitea
require="__user/gitea" __group gitea
require="__group/gitea" __user_groups gitea --group gitea

export require="__user_groups/gitea"
__directory ${GITEA_CFG_DIR} --parents --owner ${USER} --group ${GROUP}
__directory ${GITEA_DIR} --parents --owner ${USER} --group ${GROUP}

# TODO looks like the application rewrites on that file with INTERNAL_TOKEN and JWT_SECRET
# example conf src https://github.com/go-gitea/gitea/blob/main/custom/conf/app.example.ini
__file ${GITEA_CFG_DIR}/app.ini --owner ${USER} --group ${GROUP} --source - <<EOF
[database]
DB_TYPE = sqlite3
[security]
INSTALL_LOCK = true
EOF

GITEA_IS_VERSION="$(cat "${__object}/explorer/gitea-version")"
GITEA_URL="https://github.com/go-gitea/gitea/releases/download/v${GITEA_SHOULD_VERSION}/gitea-${GITEA_SHOULD_VERSION}-linux-amd64.xz"
GITEA_VERSION_FILE="/usr/local/bin/.gitea.cdist.version"

# src https://github.com/go-gitea/gitea/blob/main/contrib/systemd/gitea.service
# src https://docs.gitea.io/en-us/linux-service/
__systemd_unit gitea.service \
        --enablement-state "enabled" \
        --source - <<EOF
[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
###
# Don't forget to add the database service requirements
###
#
#Requires=mysql.service
#Requires=mariadb.service
#Requires=postgresql.service
#Requires=memcached.service
#Requires=redis.service
#
###
# If using socket activation for main http/s
###
#
#After=gitea.main.socket
#Requires=gitea.main.socket
#
###
# (You can also provide gitea an http fallback and/or ssh socket too)
#
# An example of /etc/systemd/system/gitea.main.socket
###
##
## [Unit]
## Description=Gitea Web Socket
## PartOf=gitea.service
##
## [Socket]
## Service=gitea.service
## ListenStream=<some_port>
## NoDelay=true
##
## [Install]
## WantedBy=sockets.target
##
###

[Service]
# Modify these two values and uncomment them if you have
# repos with lots of files and get an HTTP error 500 because
# of that
###
#LimitMEMLOCK=infinity
#LimitNOFILE=65535
RestartSec=2s
Type=simple
User=gitea
Group=gitea
WorkingDirectory=/var/lib/gitea/
# If using Unix socket: tells systemd to create the /run/gitea folder, which will contain the gitea.sock file
# (manually creating /run/gitea doesn't work, because it would not persist across reboots)
#RuntimeDirectory=gitea
ExecStart=/usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=gitea HOME=/home/gitea GITEA_WORK_DIR=/var/lib/gitea
# If you install Git to directory prefix other than default PATH (which happens
# for example if you install other versions of Git side-to-side with
# distribution version), uncomment below line and add that prefix to PATH
# Don't forget to place git-lfs binary on the PATH below if you want to enable
# Git LFS support
#Environment=PATH=/path/to/git/bin:/bin:/sbin:/usr/bin:/usr/sbin
# If you want to bind Gitea to a port below 1024, uncomment
# the two values below, or use socket activation to pass Gitea its ports as above
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE
###

[Install]
WantedBy=multi-user.target
EOF

export require="__systemd_unit/gitea.service"

if [ "${GITEA_SHOULD_VERSION}" != \
  "${GITEA_IS_VERSION}" ]; then
  GITEA_TMP_PATH="/tmp/gitea-${GITEA_SHOULD_VERSION}"
  # shellcheck disable=SC2059
  __download ${GITEA_TMP_PATH}.xz \
    --url "${GITEA_URL}" \
    --download remote \
    --sum "${GITEA_CHECKSUM}"

  require="__download${GITEA_TMP_PATH}.xz" \
    __unpack "${GITEA_TMP_PATH}.xz" \
      --destination "${GITEA_TMP_PATH}" \
      --onchange "$(cat <<EOF
chown -R ${USER}:${GROUP} "${GITEA_TMP_PATH}"
chmod +x "${GITEA_TMP_PATH}"
cp -f "${GITEA_TMP_PATH}" /usr/local/bin/gitea
EOF
)"
  printf "%s" "${GITEA_SHOULD_VERSION}" | \
    require="${require} __unpack${GITEA_TMP_PATH}.xz" __file \
      "${GITEA_VERSION_FILE}" \
        --source "-"
fi
