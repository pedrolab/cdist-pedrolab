You can define scrape-interval to set typical scrape and evaluation interval
configured in Prometheus. Optional parameter. Defaults to 15s (such as in
prometheus). For some dashboards with lower interval scrape you will need to
adjust the interval in a way such as

    sed -i '/      "legend": {/i \ \ \ \ \ \ "interval": "2s",' mydashboard.json

You can specify prometheus datasource url, but it defaults to
http://localhost:9090

You can upload your own datasource file
