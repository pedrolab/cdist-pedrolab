cdist-type__evilham_single_binary_service(7)
============================================

NAME
----
cdist-type__evilham_single_binary_service - Setup a single-binary service


DESCRIPTION
-----------
This type is designed to easily deploy and configure a single-binary service
named `${__object_id}`.

A good example of this are Prometheus exporters.

This type makes certain assumptions that might not be correct on your system.
If you need more flexibility, please get in touch and provide a use-case
(and hopefully a backwards-compatible patch).

This type will place the downloaded binary and, if requested, other extra
binaries in the path provided in the bin-dir parameter

If a `--config-file-source` is provided, it will be placed under:
`/etc/${__object_id}.conf`.

TODO (patches welcome!):
- It currently only supports `.tar.gz` archives.
- It currently only supports systemd units.
- Does not handle properly BSD-systems (wheel group, /usr/local/etc, systemd)


REQUIRED PARAMETERS
-------------------
checksum
    This will be passed verbatim to `__download(7)`.
    Use something like `sha256:...`.

url
    This will be passed verbatim to `__download(7)`.

version
    This type will use a thumbstone file with a "version" number to track
    whether or not a service must be updated.
    This thumbstone file is placed under
    `/usr/local/bin/.${__object_id}.cdist.version`.


BOOLEAN PARAMETERS
------------------
unpack
    If present, the contents of `--url` will be treated as an archive to be
    unpacked with `__unpack(7)`.
    See also `--unpack-args` and `--extra-binary`.

do-not-manage-user
    Always considered present when `--user` is `root`.
    If present, the user in `--user` will not be managed by this type with
    `__user`, this means it *must* exist beforehand when installing the service
    and it will not be removed by this type.


OPTIONAL PARAMETERS
-------------------
service-config
	If present, this file's contents will be placed as the service config to be
	enabled, if not, an opinionated template is used.
	If `-` is passed, service-config can be loaded in this type's `stdin`

user
    The user under which the service will run. Defaults to `root`.
    If this user is not `root` and `--do-not-manage-user` is not present,
    this user will be created or removed as per the `--state` parameter.

group
    The group under which the service will run. Defaults to `--user`.

state
    Whether the service is to be `present` (default) or `absent`.
    When `absent`, this type will clean any binaries listed in `--extra-binary`
    and also the config file as described in `--config-file-source`.

binary
    This will be the binary name. Defaults to `${__object_id}`.
    If `--unpack` is used, a binary with this name must be unpacked.
    Otherwise, the contents of `--url` will be placed under this binary name.

service-args
    Any extra arguments to pass along with `--service-exec`. Beware that any
    service-args having the format `--config=/etc/foo.cfg` should be
    represented in the following way `--service-exec='--config=/etc/foo.cfg'`

service-exec
    The executable to use for this service.
    Defaults to `/usr/local/bin/BINARY_NAME` where `BINARY_NAME` is the
    resulting value of `--binary`.

service-description
    The service description to be used in, e.g. the systemd unit file.
    Defaults to `cdist-managed '${__object_id}' service`.

unpack-args
    Only has an effect if `--unpack` is used.
    These arguments will be passed verbatim to `__unpack(7)`.
    Very useful as this type assumes the archive does not have the binaries in
    subdirectories; that can be worked around with
    `--unpack-args '--tar-strip 1'`.

bin-dir
	If present, places the downloaded binary and, if requested, other extra
	binaries in the path provided.
	By default it uses the location `/usr/local/bin`


OPTIONAL MULTIPLE PARAMETERS
----------------------------
extra-binary
    Only useful with `--unpack`.
    If passed, these binaries will also be installed when `--state` is `present`
    and removed when `--state` is `absent`.
    Handle with care :-).


EXAMPLES
--------

TODO adapt example to new design

.. code-block:: sh

	# Install and enable the ipmi_exporter service
	#   The variables are defined in the manifest previously
	__evilham_single_binary_service ipmi_exporter \
		--user "${USER}" \
		--service-args ' --config.file=/etc/ipmi_exporter.conf' \
		--version "${SHOULD_VERSION}" \
		--checksum "${CHECKSUM}" \
		--url "${DOWNLOAD_URL}" \
		--state "present" \
		--unpack \
		--unpack-args "--tar-strip 1" \
		--config-file-source '-' <<-EOF
	# Remotely managed, changes will be lost
	# [...] config contents goes here
	EOF

	# Remove the ipmi_exporter service along with the user and its config
	__evilham_single_binary_service ipmi_exporter \
		--user "${USER}" \
		--version "${SHOULD_VERSION}" \
		--checksum "${CHECKSUM}" \
		--url "${DOWNLOAD_URL}" \
		--state "absent"

	# Same, but the service was using my user! Let's not delete that!
	__evilham_single_binary_service ipmi_exporter \
		--user "evilham" \
		--do-not-manage-user \
		--version "${SHOULD_VERSION}" \
		--checksum "${CHECKSUM}" \
		--url "${DOWNLOAD_URL}" \
		--state "absent"


SEE ALSO
--------
- `__download(7)`
- `__unpack(7)`


AUTHORS
-------
Evilham <contact@evilham.com>


COPYING
-------
Copyright \(C) 2021 Evilham.
